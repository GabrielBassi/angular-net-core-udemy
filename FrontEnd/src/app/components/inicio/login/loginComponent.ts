import { LoginService } from 'src/app/services/login.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Usuario } from 'src/app/models/usuario';
import { ToastrService } from 'ngx-toastr'; 
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  login: FormGroup;
  loading = false;
  constructor(private fb: FormBuilder, private toastr: ToastrService, 
              private router: Router, private loginService: LoginService) {
    this.login = this.fb.group({
      usuario: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  ngOnInit(): void {
  }

  log(): void {
    const usuario:Usuario ={
      nombreUsuario: this.login.value.usuario,
      password: this.login.value.password
    }

    this.loading = true;
    this.loginService.login(usuario).subscribe(data => {
    this.loading = false;
    this.loginService.setLocalStorage(data.token);
    this.router.navigate(["/dashboard"]);
    this.toastr.success(usuario.nombreUsuario, "Bienvenido"), {
      positionClass: 'toast-bottom-full-width'
    };
    },error => {
      console.log(error);
      this.loading = false;
      this.toastr.error("Usuario o contraseña incorrectos", "Error"), {
        positionClass: 'toast-bottom-full-width'
      };
      this.login.reset();
      this.loading = false;
    })
  }
}
