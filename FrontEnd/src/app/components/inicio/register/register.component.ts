import { Usuario } from 'src/app/models/usuario';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  register: FormGroup; 
  loading = false;
  constructor(private fb: FormBuilder, private usuarioService: UsuarioService, 
              private router: Router, private toastr: ToastrService) { 
    this.register = this.fb.group( {
      usuario: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(4)]],
      confirmPassword: ['']
    }, { validator: this.checkPassword });
  }

  ngOnInit(): void {
  }

  checkPassword(group: FormGroup): any {
    const pass = group.get('password')?.value;
    const confirmPass = group.get('confirmPassword')?.value;
    return pass === confirmPass ? null : { notSame: true };
  }
  
  registrarUsuario(): void {
    console.log(this.register)

    const usuario: Usuario = {
      nombreUsuario: this.register.value.usuario,
      password: this.register.value.password,
    }
    this.loading = true;
    this.usuarioService.saveUser(usuario).subscribe(data => {
      console.log("Data", data);
      this.toastr.success("Usuario registrado!", "El usuario " + usuario.nombreUsuario + " fue registrado con éxito", {
        positionClass: 'toast-bottom-center'
      });
      this.router.navigate(['/inicio/login']);
    }, error => {
      this.loading = false;
      this.toastr.error(error.error.message," Error!");
      this.register.reset();
    })
  }
}

