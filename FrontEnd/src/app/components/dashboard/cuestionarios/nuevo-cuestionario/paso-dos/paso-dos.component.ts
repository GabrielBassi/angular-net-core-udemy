import { CuestionarioService } from 'src/app/services/cuestionario.service';
import { Component , OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { Pregunta } from 'src/app/models/pregunta';
import { Cuestionario } from 'src/app/models/cuestionario';
import { tap, catchError, finalize } from 'rxjs/operators';

@Component({
  selector: 'app-paso-dos',
  templateUrl: './paso-dos.component.html',
  styleUrls: ['./paso-dos.component.scss']
})

export class PasoDosComponent implements OnInit {
  tituloCuestionario: string;
  descripcionCuestionario: string;
  listPreguntas: Pregunta[] = [];
  loading = false;

  constructor(private cuestionarioService: CuestionarioService,
              private toastr: ToastrService,
              private router: Router) { }

  ngOnInit(): void {
    this.tituloCuestionario = this.cuestionarioService.tituloCuestionario;
    this.descripcionCuestionario = this.cuestionarioService.descripcionCuestionario;
  }

  guardarPregunta(pregunta: Pregunta):void {
    this.listPreguntas.push(pregunta);
  }

  eliminarPregunta(index: number):void{
    this.listPreguntas.splice(index, 1);
  }

  guardarCuestionario(): void {
    const cuestionario: Cuestionario = {
      nombre: this.tituloCuestionario,
      descripcion: this.descripcionCuestionario,
      listPreguntas: this.listPreguntas
    };
    this.loading = true;



    this.cuestionarioService.guardarCuestionario(cuestionario)
      .pipe(
        tap((data) => {
          console.log("data", data);
          this.toastr.success('El cuestionario fue registrado con éxito', 'Cuestionario Registrado');
        }),
        catchError(error => {
          console.error('Error:', JSON.stringify(error));
          this.toastr.error('Oops... ¡Ocurrió un error!', 'Error');
          return []; // Retorna un observable vacío para que el flujo continúe
        }),
        finalize(() => {
          this.router.navigate(['/dashboard']);
          this.loading = false;
        })
      )
      .subscribe();
    
  }
}
