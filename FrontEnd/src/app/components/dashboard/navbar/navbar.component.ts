import { LoginService } from 'src/app/services/login.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit{
  constructor(private LoginService: LoginService, private router: Router) {}

  ngOnInit(): void {
  }

  logout():void {
    this.LoginService.removeLocalStorage();
    this.router.navigate(["/inicio"]);
  }
}
