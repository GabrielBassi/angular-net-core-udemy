import { Injectable } from '@angular/core';
import { Cuestionario } from './../models/cuestionario';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CuestionarioService {

  myAppUrl: string;
  myApiUrl: string;
  tituloCuestionario: string;
  descripcionCuestionario: string;

  constructor(private http: HttpClient) {
    this.myAppUrl = environment.endpoint;
    this.myApiUrl = '/api/Cuestionario';
  }

  guardarCuestionario(cuestionario: Cuestionario): Observable<any> {
    let post = this.http.post(this.myAppUrl + this.myApiUrl, cuestionario);
    return post;
  }
}
