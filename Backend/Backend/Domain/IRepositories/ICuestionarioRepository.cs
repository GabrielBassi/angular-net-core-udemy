﻿using BackEnd.Domain.Models;

namespace Backend.Domain.IRepositories
{
    public interface ICuestionarioRepository
    {
        Task CreateCuestionario(Cuestionario cuestionario);
    }
}
