﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using BackEnd.Domain.Models;

namespace Backend.Domain.Models
{
    public class Pregunta
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varChar(100)")]
        public string? Descripcion { get; set; }

        public int CuestionarioId { get; set; }

        public Cuestionario? Cuestionario { get; set; }

        public List<Respuesta>? ListRespuestas { get; set; }
    }
}
