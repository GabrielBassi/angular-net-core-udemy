﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Domain.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        [Required]
        [Column(TypeName = "varChar(30)")]
        public string? NombreUsuario { get; set; }
        [Required]
        [Column(TypeName = "varChar(50)")]
        public string? Password { get; set; }
    }
}
