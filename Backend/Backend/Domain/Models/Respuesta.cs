﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Backend.Domain.Models
{
    public class Respuesta
    {
        public int Id { get; set; }

        [Required]
        [Column(TypeName = "varChar(150)")]

        public string? Descripcion { get; set; }

        [Required]
        public bool EsCorrecta { get; set; }

        public int PreguntaId { get; set;}

        public Pregunta? Pregunta { get; set;}
    }
}
