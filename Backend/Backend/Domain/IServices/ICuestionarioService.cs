﻿using BackEnd.Domain.Models;

namespace Backend.Domain.IServices
{
    public interface ICuestionarioService
    {
        Task CreateCuestionario(Cuestionario cuestionario);
    }
}
