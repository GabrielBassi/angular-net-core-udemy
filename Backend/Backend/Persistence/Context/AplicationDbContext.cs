﻿using Backend.Domain.Models;
using BackEnd.Domain.Models;
using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.EntityFrameworkCore;

namespace Backend.Persistence.Context
{
    public class AplicationDbContext : DbContext
    {
        public DbSet<Usuario>Usuario { get; set; } //Mapea la entidad usuario a la tabla
        public DbSet<Cuestionario> Cuestionario { get; set; }
        public DbSet<Pregunta> Pregunta { get; set; }
        public DbSet<Respuesta> Respuesta { get; set; }

        public AplicationDbContext(DbContextOptions<AplicationDbContext> options) : base(options)
        {

        }

    }
}
