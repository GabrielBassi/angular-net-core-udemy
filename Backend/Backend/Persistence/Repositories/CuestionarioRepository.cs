﻿using Backend.Domain.IRepositories;
using Backend.Persistence.Context;
using BackEnd.Domain.Models;

namespace Backend.Persistence.Repositories
{
    public class CuestionarioRepository : ICuestionarioRepository
    {
        private readonly AplicationDbContext _context;

        public CuestionarioRepository(AplicationDbContext context)
        {
            _context = context;
        }

        public async Task CreateCuestionario(Cuestionario cuestionario)
        {
            _context.Add(cuestionario);

            await _context.SaveChangesAsync();
        }
    }
}
